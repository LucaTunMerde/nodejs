var express = require('express')
var router = express.Router()
var db = require('../db.js');
const ObjectId = require('mongodb').ObjectId;

// define the home page route
router.get('/', function(req, res) {
    db.get().collection('img').find({}).toArray(function(err, response) {

        res.render('index', {
            title: 'Accueil',
            imgs: response
        });
    });
});

// Renders all images in database
router.get('/list', function(req, res) {
    db.get().collection('img').find({}).toArray(function(err, imgs) {
        res.render('list', {
            imgs: imgs
        });
    });
});

// Add an image vue
router.get('/add', function(req, res) {
    res.render('add', {
        title: 'Ajouter une photo'
    });
});

// Adds image to the database
router.post('/img_add', function(req, res) {
    newImg = {
        title: req.body.titleImg,
        url: req.body.urlImg
    }
    db.get().collection("img").insertOne(newImg, function(err, img) {
        res.redirect('/list')
    });
});

// Edit requested image
router.get('/list/:id/edit', function(req, res) {
    db.get().collection("img").findOne({
        _id: ObjectId(req.params.id)
    }, function(err, img) {
        // console.log(img.title);
        res.render('edit', {
            title: 'Modifier la photo',
            imgTitle: img.title,
            imgUrl: img.url,
            imgID: img._id
        });
    });
});

// Update image in database
router.post('/img_update', function(req, res) {
    // console.log(req.body);
    db.get().collection("img").findOne({
        _id: ObjectId(req.body._id)
    }, function(err, img) {
        newImg = img;
        newImg.title = req.body.titleImg;
        newImg.url = req.body.urlImg;
        console.log(newImg);
        db.get().collection('img').updateOne({
            _id: ObjectId(req.body._id)
        }, {
            $set: newImg
        }, function(prb, newimg) {
            res.redirect('/list');
        });
    });
});

// Delete requested image
router.post('/delete/:id', function(req, res) {
    db.get().collection("img").deleteOne({
        _id: ObjectId(req.params.id)
    }, function(err, img) {
        res.redirect('/list')
    });
});




module.exports = router
