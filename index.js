const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');

let app = express();


/* Db config */
const db = require('./db.js');
db.connect(() => {
    app.listen(process.env.PORT || 5555, function (){
        console.log(`Database connection opened`);
    });
});


/* App config */
app.set('appName', 'Bonjour');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.urlencoded());


/* Routes */
var index = require('./routes/index.js');

app.use('', index);



//app.use("/js", express.static(path.join(__dirname, "/js")));

/* Server config*/
http.createServer(app)
    .listen(
        app.get('port'),
        () => {
            console.log("Server up")
        });
