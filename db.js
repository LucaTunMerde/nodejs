const MongoClient = require('mongodb').MongoClient;
const mongoDbUrl = 'mongodb://127.0.0.1:27017';
const client = new MongoClient(mongoDbUrl);
let mongodb;

function connect(callback){
    client.connect((err, db) => {
        mongodb = client.db("nodedb");
        callback();
    });
}
function get(){
    return mongodb;
}

function close(){
    client.close();
    console.log("Database connection closed")
}

module.exports = {
    connect,
    get,
    close
};
